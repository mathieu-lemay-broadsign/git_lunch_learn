CC=gcc
CFLAGS=-g -pedantic -std=c99 -Wall -Wshadow -Wpointer-arith -Wcast-qual -Wstrict-prototypes -Wmissing-prototypes
LDFLAGS=

all: main.c
	$(CC) -o bf $(CFLAGS) main.c $(LDFLAGS)

clean:
	$(RM) bf
