#include <stdio.h>
#include <stdlib.h>

/* Should be at least 0x7530 (30 000) */
#define ARRAY_SIZE 0x8000

/* Prototypes */
void run(unsigned char *p_data, unsigned char *p_instr, size_t program_size);

void run(unsigned char *p_data, unsigned char *p_instr, size_t program_size) {
    char c;
    unsigned char *c_data, *c_instr;

    c_data = p_data;
    c_instr = p_instr;

    while (c_instr - p_instr < program_size) {
        c = *c_instr;

        switch (c) {
        case '>':
            ++c_data;
            if (c_data >= p_data + ARRAY_SIZE) {
                fprintf(stderr, "Error: Pointer can't go over 0x%x\n", ARRAY_SIZE);
                exit(EXIT_FAILURE);
            }
            break;
        case '<':
            --c_data;
            if (c_data < p_data) {
                fprintf(stderr, "Error: Pointer can't go under 0\n");
                exit(EXIT_FAILURE);
            }
            break;
        }

        c_instr++;
    }
}

int main(int argc, char *argv[]) {
    FILE *file;
    unsigned char *p_data, *p_instr;
    size_t program_size;

    if (argc < 2) {
        printf("Usage: %s file\n", argv[0]);
        return EXIT_FAILURE;
    }

    file = fopen(argv[argc - 1], "r");
    if (file == NULL) {
        fprintf(stderr, "Error: %s: File not found\n", argv[1]);
        return EXIT_FAILURE;
    }

    fseek(file, 0, SEEK_END);
    program_size = ftell(file);
    fseek(file, 0, SEEK_SET);

    p_data = (unsigned char*)calloc(1, ARRAY_SIZE);
    p_instr = (unsigned char*)malloc(program_size);

    if (fread(p_instr, program_size, sizeof(char), file) != sizeof(char)) {
        fprintf(stderr, "Error: Unable to read from file: %s\n", argv[1]);
        return EXIT_FAILURE;
    }

    fclose(file);

    run(p_data, p_instr, program_size);

    free(p_instr);
    free(p_data);
    return EXIT_SUCCESS;
}
