#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

/* Should be at least 0x7530 (30 000) */
#define ARRAY_SIZE 0x8000

#define TRUE 1
#define FALSE 0

/* Prototypes */
void run(unsigned char *p_data, unsigned char *p_instr, size_t program_size);
void decr_value(unsigned char *ptr);
void incr_value(unsigned char *ptr);
void fatal(const char *fmt, ...);

void run(unsigned char *p_data, unsigned char *p_instr, size_t program_size) {
    int i;
    char c;
    unsigned char *c_data, *c_instr;

    c_data = p_data;
    c_instr = p_instr;

    while (c_instr - p_instr < program_size) {
        c = *c_instr;

        switch (c) {
        case '>':
            ++c_data;
            if (c_data >= p_data + ARRAY_SIZE) {
                fatal("Error: Pointer can't go over 0x%x\n", ARRAY_SIZE);
            }
            break;
        case '<':
            --c_data;
            if (c_data < p_data) {
                fatal("Error: Pointer can't go under 0\n");
            }
            break;
        case '+':
            incr_value(c_data);
            break;
        case '-':
            decr_value(c_data);
            break;
        case '.':
            putchar(*c_data);
            break;
        case ',':
            *c_data = getchar();
            break;
        case '[':
            if (*c_data == 0) {
                i = 0;
                c = *(++c_instr);
                while (c != ']' || i != 0) {
                    if (c == '[')
                        i++;
                    if (c == ']')
                        i--;
                    c = *(++c_instr);
                }
            }
            break;
        case ']':
            if (*c_data != 0) {
                i = 0;
                c = *(--c_instr);
                while (c != '[' || i != 0) {
                    if (c == '[')
                        i--;
                    if (c == ']')
                        i++;
                    c = *(--c_instr);
                }
            }
            break;
        }

        c_instr++;
    }
}

void decr_value(unsigned char *ptr) {
    --(*ptr);
}

void incr_value(unsigned char *ptr) {
    ++(*ptr);
}

void fatal(const char *fmt, ...) {
    char msg[1024];
    va_list va;

    va_start(va, fmt);
    vsnprintf(msg, sizeof msg, fmt, va);
    fprintf(stderr, "%s", msg);

    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[]) {
    FILE *file;
    unsigned char *p_data, *p_instr;
    size_t program_size;

    if (argc < 2) {
        fatal("Usage: %s file\n", argv[0]);
    }

    file = fopen(argv[argc - 1], "r");
    if (file == NULL) {
        fatall("Error: %s: File not found\n", argv[1]);
    }

    fseek(file, 0, SEEK_END);
    program_size = ftell(file);
    fseek(file, 0, SEEK_SET);

    p_data = (unsigned char*)calloc(1, ARRAY_SIZE);
    p_instr = (unsigned char*)malloc(program_size);

    if (fread(p_instr, program_size, sizeof(char), file) != sizeof(char)) {
        fprintf(stderr, "Error: Unable to read from file: %s\n", argv[1]);
        return EXIT_FAILURE;
    }

    fclose(file);

    run(p_data, p_instr, program_size);

    free(p_instr);
    free(p_data);
    return EXIT_SUCCESS;
}
