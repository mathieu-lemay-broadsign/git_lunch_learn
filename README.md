[Configuration]: # ({{{)

# Configuration
Set name and email
```bash
git config --global user.name "Your Name"
git config --global user.email "your_email@whatever.com"
```

Ensure git 2.0+ push default
```bash
git config --global push.default simple
```

Make conflict resolution much easier
```bash
git config --global merge.conflictStyle diff3
```

[//]: # (}}})

[Clone]: # ({{{)

# Cloning
```bash
cd ~

git clone https://mathieu-lemay@bitbucket.org/mathieu-lemay/git_lunch_learn.git git_lunch_learn
cd git_lunch_learn
```

[//]: # (}}})

[Log]: # ({{{)

# Log
Show commits log
```bash
git log
```

Show commits log in a condensed manner
```bash
git log --oneline
```

Show commits log with changes
```bash
git log -p
```

Show commits log from a specific branch or commit
```bash
git log <branch|commit>
```

Show changes of a specific commit
```bash
git show <commit>
```

Show file at a specific commit
```bash
git show <commit>:main.c
```

[//]: # (}}})

[Branches]: # ({{{)

# Branches
## Listing branches
Local branches
```bash
git branch
```

Remote branches
```bash
git branch -r
```

All branches
```bash
git branch -a
```

## Create branches
Create branch from current HEAD
```bash
git branch foo
```

Create branch from other branch
```bash
git branch bar <branch>
```

Create branch from a commit
```bash
git branch baz <commit>
```

## Delete branch
Safely delete a branch
```bash
git branch -d foo
git branch -d bar  # Won't work for this one
```

Force delete a branch
```bash
git branch -D bar baz
```

[//]: # (}}})

[Checkout]: # ({{{)

# Checkout
## Switch branch
```bash
git checkout <branch>
git checkout master
```

## Checkout a commit
Checkout the commit
```bash
git checkout <commit>
```

Go back to master branch
```bash
git checkout master
```

## Create branch / Checkout shortcut
Create and checkout a branch in one step
```bash
git checkout -b foo
git checkout master
git branch -d foo
```

## Restore working tree file
Add some changes to the file
```bash
echo foobar > main.c
```

See our file has changes
```bash
git status
git diff
```

Restore commited version of the file
```bash
git checkout main.c
```

Changes are gone
```bash
git status
```

[//]: # (}}})

[Stage/Commit]: # ({{{)

# Stage / Commit
First, let's create a branch and add some changes
```bash
cat main.v2.c > main.c
```

Show changes
```bash
git diff
```

## Stage a file
Stage our file
```bash
git add main.c
```

Add some new changes
```bash
echo foobar >> main.c
```

Show unstaged changes
```bash
git diff
```

Show staged changes
```bash
git diff --staged
```

Revert *unstaged* changes
```bash
git checkout main.c
```

Unstage the file
```bash
git reset main.c
```

Interactive stage
```bash
git add -p
```

## Commit the changes
Commit
```bash
git commit
```

Commit with message
```bash
git commit -m 'My commit message'
```

Commit everything unstaged
```bash
git commit -a
```

[//]: # (}}})

[Amend]: # ({{{)

# Amend
Fix our typo
```bash
sed -i.old 's/fatall/fatal/' main.c
rm main.c.old
```

Stage our fix
```bash
git add main.c
```

Amend our last commit
```bash
git commit --amend
```

[//]: # (}}})

[Reset]: # ({{{)

# Reset
We just realized we did all that work in the master branch.  Let's move it.

Create the branch from current HEAD
```bash
git branch add-fatal-function
```

Reset to commit before HEAD
```bash
git reset HEAD^
```

Changes are still present
```bash
git diff
```

Hard reset to HEAD
```bash
git reset --hard HEAD
```

We could have done it in one single step
```bash
# DO NOT EXECUTE
git reset --hard HEAD^
```

[//]: # (}}})

[Stash]: # ({{{)

# Stash
First, we need some changes
```bash
echo foo >> main.c
```

Save it to stash
```bash
git stash
```

Some more changes
```bash
echo bar >> main.c
```

Save it to stash with some message
```bash
git stash save 'Some message'
```

List stash
```bash
git stash list
```

Show info on a stash. The `-p` flag will show the full diff contained in the stash.
```bash
git stash show [-p] stash@{n}
```

Apply top of stash
```bash
git stash apply
```

Drop top of stash
```bash
git stash drop
```

We can also pop the top of the stash (apply + drop). Git won't let us apply stashed changes if there are local changes.

```bash
git stash pop
```

Note, if there are any conflicts, the drop will not be done

[//]: # (}}})

[Merge]: # ({{{)

# Merge
## Preparation
To make things easier, we need to checkout all branches locally
```bash
git checkout improvement/add-support-for-comments
git checkout improvement/incr-decr-functions
git checkout instruction/incr-decr
git checkout instruction/loop
git checkout instruction/read-print
```

## Merging
Merge branch `instruction/incr-decr` in branch `master`. This will create a fast-forward merge.
```bash
git checkout master
git merge instruction/incr-decr
```

Merge branch `improvement/incr-decr-functions` in branch `master`. This will also create a fast-forward merge.
```bash
git merge improvement/incr-decr-functions
```

Merge branch `instruction/read-print` in master brach. This will cause a conflict! Let's abort.
```bash
git merge instruction/read-print
git status
git merge --abort
git status
```

Merge branch `instruction/read-print` in master brach. This will cause a conflict!
This time, we will fix it.
```bash
git merge instruction/read-print
git status
vim main.c
git add main.c
git commit
```

[//]: # (}}})

[Rebase]: # ({{{)

# Rebase

## Regular rebase

Rebase branch `instruction/loop` on top of branch `master`. This will also cause a conflict.
We can abort it the same way we did for the merge.
```bash
git checkout instruction/loop
git rebase master
git status
git rebase --abort
```

Rebase branch `instruction/loop` on top of branch `master`. This will also cause a conflict.
This time, we will fix it.
```bash
git rebase master
git status
vim main.c
git add main.c
git status
git rebase --continue
```

Let's merge it in `master`. Since that branch now has master for base, the merge will be a fast-forward.
```bash
git checkout master
git merge instruction/loop
```

## Interactive rebase
Rebase branch `improvement/add-support-for-comments` on top of branch `master`.
We will take this opportunity to drop some commits.
```bash
git checkout improvement/add-support-for-comments
git rebase -i master
```

Your editor of choice should open. Notice the options offered by git.
Let's remove the first and third line, save and quit.
The process will resume and there should be no conflicts.
```bash
git log
```

We could now merge it in `master` without conflict. Instead, let's see how we could have done this with...

[//]: # (}}})

[Cherry Pick]: # ({{{)

# Cherry pick
First, we cancel our changes to the `improvement/add-support-for-comments` branch. And find the commit we want.
```bash
git reset --hard origin/improvement/add-support-for-comments
git log --oneline
```

Take that commit and apply it to master.
```bash
git checkout master
git cherry-pick <hash>
git log
```

[//]: # (}}})
